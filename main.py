import requests as r
import datetime
import config as c

URL = 'https://trackapi.nutritionix.com/v2/natural/exercise'

HEADERS = {
    "x-app-id": c.APP_ID,
    "x-app-key": c.API_KEY,
    "Content-Type": "application/json"
}
BODY = {
    "query": input(str(">>")),
    "gender": "male",
    "weight_kg": 72.5,
    "height_cm": 175.5,
    "age": 24
}

request = r.post(url=URL, json=BODY, headers=HEADERS)
params = request.json()

# SHEETY:
today = datetime.datetime.now().strftime("%d/%m/%Y")
now_time = datetime.datetime.now().strftime("%X")
sheety_body = {}
for exercise in params['exercises']:
    sheety_body = {
        "workout": {
            "date": today,
            "time": now_time,
            "exercise": exercise['name'],
            "duration": exercise['duration_min'],
            "calories": exercise["nf_calories"]
        }
    }
SHEETY = "https://api.sheety.co/1cd91060da72a8ff17e7a371fe97dd75/trening/workouts"

sheety_r = r.post(url=SHEETY, json=sheety_body, headers=c.HEADERS_SHEETY)
print(sheety_r.json())
